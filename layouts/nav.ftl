<ul id="menu-menu-1" class="menu" >
	<@menuTag method="tree">
		<#list menus?sort_by('priority') as menu>
			<li>

				<a <#if menu.url?? && menu.url?trim?length gt 0> href="${menu.url!}" <#if menu.name=="统计">target="_top"</#if> target="${menu.target!}" </#if>>
                    <span class="faa-parent animated-hover">
                        <#if menu.icon?? && menu.icon?trim?length gt 0>
                            <i class="${menu.icon}" aria-hidden="true"></i>
                        </#if>${menu.name}
                    </span>
					<#if menu.name=="统计"><p>sd</p></#if>
				</a>
                <#if menu.children?? && menu.children?size gt 0>
					<ul class="sub-menu">
						<#list menu.children?sort_by('priority') as child>
							<li>
								<#if menu.name=="统计"><p>sdddd</p></#if>
								<a href="${child.url!}" <#if child.name=="统计">target="_top"</#if> target="${child.target!}">
								<#if child.icon?? && child.icon?trim?length gt 1>
									<span class="faa-parent animated-hover">
										<i class="${child.icon}" aria-hidden="true"></i>
									</span>
									</#if>${child.name}
                                </a>
							<li>
						</#list>
					</ul>
			    </#if>
			</li>

		</#list>
	</@menuTag>
</ul>