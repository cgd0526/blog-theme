<style>
    .note.warning {
        background-color: #fdf8ea;
        border-left-color: #f0ad4e;
    }
    .note.primary {
        background-color: #f5f0fa;
        border-left-color: #6f42c1;
    }
    .note.info {
        background-color: #eef7fa;
        border-left-color: #428bca;
    }
    .note {
        position: relative;
        padding: 15px;
        margin-top: 10px;
        margin-bottom: 10px;
        border: initial;
        border-left: 3px solid #eee;
        background-color: #f9f9f9;
        border-radius: 3px;
    }
    .links-box {
        margin: 0 auto;
    }
    /* 美化 hr 样式 */
    hr {
        position: relative;
        margin: 2rem auto;
        width: calc(100% - 4px);
        border: 2px dashed #a4d8fa;
        background: #fff;
    }

    hr {
        box-sizing: content-box;
        height: 0;
        overflow: visible;
    }

    hr:before {
        position: absolute;
        top: -10px;
        left: 5%;
        z-index: 1;
        color: #49b1f5;
        content: '\f0c4';
        font: normal normal normal 14px/1 FontAwesome;
        font-size: 20px;
        -webkit-transition: all 1s ease-in-out;
        -moz-transition: all 1s ease-in-out;
        -o-transition: all 1s ease-in-out;
        -ms-transition: all 1s ease-in-out;
        transition: all 1s ease-in-out;
    }

    hr:hover::before{
        left: 95%;
    }
    div.note {
        position: relative;
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
        padding: 0.6rem 16px 0.5rem 16px;
        padding-left: calc(16px + 16px);
        border-radius: 4px;
        background: #f6f6f6;
        border-left: 4px solid #767676;
    }
</style>
<article>
    <#if is_sheet??>
        ${sheet.formatContent!}
    </#if>
    <div class="links-box">
        <div class="links">
            <@linkTag method="listTeams">
                <#list teams as item>
                    ${((item.team!'')?length>0)?string((item.team!''), '小伙伴们')}
                    <ul class="link-items fontSmooth">
                        <#list item.links?sort_by('priority')?reverse as link>
                            <li class="link-item">
                                <a class="link-item-inner effect-apollo" href="${link.url!}" title="${link.name!}" target="_blank">
                                    <img class="lazyload" data-src="${link.logo!}" src="${res_base_url!}/source/images/svg/loader/trans.ajax-spinner-preloader.svg" onerror="imgError(this)">
                                    <span class="sitename">${link.name!}</span>
                                    <div class="linkdes">${link.description!}</div>
                                </a>
                            </li>
                        </#list>
                    </ul>
                </#list>
            </@linkTag>
        </div>
    </div>
    <hr />
    <div align="center"><img src="https://image.bestzuo.cn/images/20200701175738.gif!getwebp" width="300"></div>
    <p>🍜   <strong>友链申请要求</strong>：</p>
    <div class="note ">
        <p>
            1.<strong>网站证书未过期，谢绝资源网站~</strong> <br>
            2.<strong>申请前请将我添加至您的友链页面哦~</strong> <br>
            3.<strong>原则上要求您的<span style="color:orange">博客主页</span>被百度或者 Google 等<span style="color:#9929ff">搜索引擎收录</span></strong> <br>
            4.<strong>新站点（建站时间不超过三个月）请维护一段时间后再来申请，不接受一时起兴的博客</strong>
        </p>
    </div>
    <p>🍭  <strong>友链申明</strong>：</p>
    <div class="note no-icon">
        <p><a href="https://www.chenmx.net/?p=337" target="_blank" title="自动友链管理工具">1.自动清理已阵亡友链，请爱护你的小站。如更换了链接信息请至评论区留言！</a><br> 2.没事多来留言，小站永远欢迎😘</p>
    </div>
    <p>🍉  <strong>申请格式</strong>：</p>
    <blockquote>
        <p>昵称： 晓果冻<br>链接： <a href="https://www.chenmx.net">https://www.chenmx.net</a><br>头像：<a href="https://chen-1302214763.cos.ap-beijing.myqcloud.com/blog.jpg" target="_blank" rel="noopener">https://chen-1302214763.cos.ap-beijing.myqcloud.com/blog.jpg</a><br>描述： 是你呀，晓果冻！</p>
    </blockquote>
    <div class="note warning"><p>将对应内容替换为你的信息并保证头像和链接为<strong>https</strong>开头，发送至留言区即可👌</p></div>

    <section id="comments" class="comments">
        <div class="comments-main">
            <hr />
            <h3>友链交换留言区</h3>
            <halo-comment id="131" type="post" :configs="configs" />
        </div>
    </section>
</article>